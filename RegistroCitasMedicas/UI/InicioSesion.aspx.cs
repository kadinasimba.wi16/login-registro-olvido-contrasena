﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RegistroCitasMedicas.DATA;

namespace RegistroCitasMedicas.UI
{
    public partial class InicioSesion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetNoStore();

        }


        protected void miBoton_Click(object sender, EventArgs e)
        {
            Response.Redirect("Formulario.aspx");

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            using (SqlConnection oConexion = new SqlConnection(Conexion.rutaconexion))
            {
                string fecha = DateTime.Now.ToString("yyyy MM dd");
                string hora = DateTime.Now.ToString("hh:mm:ss");
                //SqlCommand cmd = new SqlCommand("BuscarLogin", oConexion);

                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Connection.Open();
                //cmd.Parameters.Add("@Condicion", SqlDbType.VarChar, 50).Value = username.Text;
                //cmd.Parameters.Add("@Condicion2", SqlDbType.VarChar, 50).Value = password.Text;
                SqlCommand cmd = new SqlCommand("BuscarUsuario", oConexion);
                cmd.CommandType = CommandType.StoredProcedure;

                // Add parameters
                cmd.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = username.Text;
                cmd.Parameters.Add("@contrasenia", SqlDbType.VarChar, 255).Value = password.Text;

                // Open the connection before executing the command
                cmd.Connection.Open();



                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    if (username.Text == "admin" && password.Text == "admin")
                    {
                        Session["UsuarioLogeadoA"] = username.Text;

                        Response.Redirect("PrincipalAdmin.aspx");

                    }
                    else
                    {
                        Session["UsuarioLogeado"] = username.Text;

                        Response.Redirect("PrincipalAdmin.aspx");
                    }
                }
                else
                {

                    lblErrorMessage.Text = "Error de Usuario o Contraseña";
                }

                cmd.Connection.Close();
            }
            
        }
    }
}