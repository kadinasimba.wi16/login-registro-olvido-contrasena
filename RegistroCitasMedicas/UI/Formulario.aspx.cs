﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RegistroCitasMedicas.DATA;

namespace RegistroCitasMedicas.UI
{
    public partial class Formulario : System.Web.UI.Page
    {
        // Establecer la conexión con la base de datos
        SqlConnection oConexion = new SqlConnection(Conexion.rutaconexion);

        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = "";
            lblErrorContrasenia.Text = "";
            LeerDatos();
        }

        void Limpiar()
        {
            // Limpiar los campos del formulario
            tbCedulas.Text = "";
            tbNombres.Text = "";
            tbApellidos.Text = "";
            tbFechaNacimiento.Text = "";
            tbUsuario.Text = "";
            tbConfirmarContrasenia.Text = "";
            tbContrasenia.Text = "";
            lblError.Text = "";
            lblErrorContrasenia.Text = "";
            lblSeguro.Text = "";
            tbSeguros.Text = "";
            lblTipoSangre.Text = "";
            tbTipoSangre.Text = "";

        }

        protected void BtnRegistrar_Click(Object sender, EventArgs e)
        {
            // Validar que ningún campo esté vacío
            if (tbCedulas.Text == "" || tbNombres.Text == "" ||  tbApellidos.Text == "" || tbFechaNacimiento.Text == "" || tbUsuario.Text == "" || tbConfirmarContrasenia.Text == "" || tbContrasenia.Text == "")
            {
                lblError.Text = "Ningún campo puede quedar vacío!";
            }
            else
            {
                // Verificar que las contraseñas coincidan
                if (tbContrasenia.Text != tbConfirmarContrasenia.Text)
                {
                    lblErrorContrasenia.Text = "Las contraseñas no coinciden!";
                }
                else
                {
                    // Abrir la conexión con la base de datos
                    oConexion.Open();

                    // Verificar si el usuario ya existe en la base de datos
                    SqlCommand cmd = new SqlCommand("select count(*) from Pacientes where Usuario=@Usuario", oConexion);
                    cmd.Parameters.AddWithValue("@Usuario", tbUsuario.Text);
                    int usuario = Convert.ToInt32(cmd.ExecuteScalar());

                    string patron = "InfoToolsSV";

                    if (usuario < 1)
                    {
                        SqlCommand cmm = new SqlCommand("INSERT INTO Pacientes (Cedula, Nombres, Apellidos, Fecha_Nacimiento, Tipo_Asegurado, Tipo_Sangre, Usuario, Contrasenia) VALUES (@Cedula, @Nombres, @Apellidos, @FechaNacimiento, @TipoAsegurado, @TipoSangre, @Usuario, (EncryptByPassPhrase(@Patron, @Contrasenia)))", oConexion);
                        cmm.Parameters.AddWithValue("@Cedula", tbCedulas.Text);
                        cmm.Parameters.AddWithValue("@Nombres", tbNombres.Text);
                        cmm.Parameters.AddWithValue("@Apellidos", tbApellidos.Text);
                        cmm.Parameters.AddWithValue("@FechaNacimiento", DateTime.Parse(tbFechaNacimiento.Text)); // Assuming tbFechaNacimiento.Text contains a valid date string
                        cmm.Parameters.AddWithValue("@TipoAsegurado", tbSeguros.Text);
                        cmm.Parameters.AddWithValue("@TipoSangre", tbTipoSangre.Text); // Assuming tbTipoSangre.Text contains the blood type
                        cmm.Parameters.AddWithValue("@Usuario", tbUsuario.Text);
                        cmm.Parameters.AddWithValue("@Patron", patron); // Assuming 'patron' is defined elsewhere
                        cmm.Parameters.AddWithValue("@Contrasenia", tbContrasenia.Text); // Assuming tbContrasenia.Text contains the unencrypted password
                        cmm.ExecuteNonQuery();
                        oConexion.Close();
                        Limpiar();
                        LeerDatos(); // Assuming this method reads data from the table

                    }
                    else
                    {
                        lblError.Text = "El Usuario " + tbUsuario.Text + " ya existe!";
                        tbUsuario.Text = "";
                    }
                }
            }
        }


        // Leer los datos de la tabla Usuarios y mostrarlos en el GridView
        void LeerDatos()
        {
            SqlCommand leerdatos = new SqlCommand("Select * from Pacientes", oConexion);
            SqlDataAdapter da = new SqlDataAdapter(leerdatos);
            DataTable dt = new DataTable();
            da.Fill(dt);
            gvUsuarios.DataSource = dt;
            gvUsuarios.DataBind();
        }
    }
}
